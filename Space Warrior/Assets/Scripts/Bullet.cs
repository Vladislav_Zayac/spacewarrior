using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour, IPooledObject
{
    public ObjectsPooler.ObjectInfo.ObjectType Type => type;

    [SerializeField]
    private ObjectsPooler.ObjectInfo.ObjectType type;

    private float lifeTime = 3;
    private float currentLifeTime;
    private float speed = 5;

    public void OnCreate(Vector3 position, Quaternion rotation)
    {
        transform.position = position;
        transform.rotation = rotation;
        currentLifeTime = lifeTime;
    }

    private void Update()
    {
        transform.Translate(Vector3.up * Time.deltaTime * speed);

        if((currentLifeTime -= Time.deltaTime) < 0)
        {
            ObjectsPooler.Instance.DestoyObject(gameObject);
        }
    }
}
