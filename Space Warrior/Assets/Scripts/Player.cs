using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private GameObject _playerPrefab1 = null;
    [SerializeField] private GameObject _playerPrefab2 = null;
    [SerializeField] private GameObject _playerPrefab3 = null;
    [SerializeField] private GameObject _playerPrefab4 = null;

    [SerializeField] private ObjectsPooler.ObjectInfo.ObjectType bulletType;

    private bool _onShoot = true;
    private float _speed = 5f;
    private float _epsilon = 0.2f;
    private float _extremePoint = Screen.width / 2;

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (Input.mousePosition.x > _extremePoint)
            {
                if (2.5f - transform.position.x > _epsilon) 
                { 
                    Move(1); 
                }
            }

            if (Input.mousePosition.x < _extremePoint)
            {
                if (transform.position.x + 2.5f > _epsilon)
                {
                    Move(-1);
                }
            }
        }


        //int i = 0;
        //while (i < Input.touchCount)
        //{
        //    if (Input.GetTouch(i).position.x > Screen.width / 2)
        //    {
        //        Move(1);
        //    }

        //    if (Input.GetTouch(i).position.x < Screen.width / 2)
        //    {
        //        Move(-1);
        //    }

        //    ++i;
        //}
    }

    private void Initialize()
    {
        GameObject prefab = Instantiate(_playerPrefab1, transform.position, Quaternion.identity);
        prefab.transform.SetParent(transform);
        prefab.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
        prefab.transform.Rotate(new Vector3(-150, 0, 0));
    }

    private void Start()
    {
        Initialize();
        StartCoroutine(Shoot());
    }

    IEnumerator Shoot()
    {
        while (_onShoot)
        {
            var bullet = ObjectsPooler.Instance.GetObject(bulletType);
            bullet.GetComponent<Bullet>().OnCreate(transform.position, transform.rotation);
            yield return new WaitForSeconds(0.7f);
        }
    }

    public void Move(float horizontalInput)
    {
        transform.Translate(new Vector3(horizontalInput * Time.deltaTime * _speed, 0, 0));
    }
}
