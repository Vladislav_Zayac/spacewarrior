using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bot : MonoBehaviour
{
    [SerializeField] private GameObject _botPrefab1 = null;
    [SerializeField] private GameObject _botPrefab2 = null;
    [SerializeField] private GameObject _botPrefab3 = null;
    [SerializeField] private GameObject _botPrefab4 = null;
    [SerializeField] private GameObject _botPrefab5 = null;
    [SerializeField] private GameObject _botPrefab6 = null;

    private System.Random rnd = new System.Random();

    private void Initialize()
    {
        GameObject bot = Instantiate(prefabRandom());
        bot.transform.SetParent(transform);
        bot.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        bot.transform.Rotate(new Vector3(-100, 0, 0));
    }

    private GameObject prefabRandom()
    {
        int index = rnd.Next(0, 6);
        switch (index)
        {
            case 1: return _botPrefab1;
            case 2: return _botPrefab2;
            case 3: return _botPrefab3;
            case 4: return _botPrefab4;
            case 5: return _botPrefab5;
            case 6: return _botPrefab6;
            default: return _botPrefab1;
        }
    }

    private void Start()
    {
        Initialize();
    }
}
