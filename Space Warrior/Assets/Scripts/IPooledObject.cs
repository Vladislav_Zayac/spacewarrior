interface IPooledObject
{
    ObjectsPooler.ObjectInfo.ObjectType Type { get; }
}
